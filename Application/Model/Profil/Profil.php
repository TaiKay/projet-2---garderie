<?php
/**
 * Created by PhpStorm.
 * User: WEBENOO
 * Date: 22/11/2018
 * Time: 14:15
 */

class Profil
{
    public function __construct()
    {
    }

    public static function profil(){
        include 'vendor/autoload.php';
        $model = new Model();
        $user = $model->select('user','WHERE ID='.$_SESSION['ID']);
        $user = $user->fetch();
        $child = $model->select('child','WHERE USER_ID='.$_SESSION['ID']);
        $child = $child->fetchAll();
        $tfa = new \RobThree\Auth\TwoFactorAuth('MyCreche');
        $secret = $tfa->createSecret('160');
        $qrCode = $tfa->getQRCodeImageAsDataUri($_SESSION['user']['Email'], $secret);
        if (isset($_POST['submit'])){

            $model->insert('child','ID, Name, Firstname, Birthday, Disease, Allergy, USER_ID, USER_ROLE_ID','NULL ,\' ' .$_POST['name'].'\',\''.$_POST['fname'].'\',\''.$_POST['birth'].'\',\''.$_POST['disease'].'\',\''.$_POST['allergy'].'\',\''.$_SESSION['ID'].'\',\''.$_SESSION['role'].'\'');
            ?>
            <script>
                document.location.href="http://localhost/projet-2-garderie/profil/index";
            </script>
            <?php
        }
        if (isset($_POST['delete'])){
            $child = $model->select('child','WHERE USER_ID='.$_SESSION['ID'])->fetchAll();
            foreach ($child as $kid)

                $childPro = $model->select('child_has_professional','WHERE CHILD_ID='.$kid['ID'])->fetch();
            var_dump($childPro);
            $reservation = $model->select('reservation','WHERE CHILD_has_PROFESSIONAL_CHILD_ID='.$kid['ID'])->fetch();
            var_dump($reservation);
            if ($reservation != false) {
                $model->delete('reservation ', 'WHERE CHILD_has_PROFESSIONAL_CHILD_ID=' . $kid['ID']);
                var_dump($childPro);
                if ($childPro != false)
                    $model->delete('child_has_professional ', 'WHERE CHILD_ID=' . $kid['ID']);
                //   $db->delete('child_has_professional ','WHERE CHILD_ID='.$child['ID']);
            }elseif ($childPro != false){

                // if (isset($childPro)){
                //    $db->delete('child_has_professional','WHERE CHILD_ID='.$child['ID']);
                    $model->delete('child_has_professional ','WHERE CHILD_ID='.$kid['ID']);
                $model->delete('child ','WHERE USER_ID='.$_SESSION['ID']);


                ?>
                <script>
                    document.location.href="http://localhost/projet-2-garderie/administration/manage";
                </script>
                <?php
            }else{
                var_dump($childPro);

                $model->delete('child ','WHERE USER_ID='.$_SESSION['ID']);

            ?>
            <script>
                document.location.href="http://localhost/projet-2-garderie/profil/index";
            </script>
            <?php

            }
        }
        if(isset($_POST['submitInfo'])) {
            if(isset($_POST['name']) && isset($_POST['firstname']) && isset($_POST['address']) && isset($_POST['others'])) {
                $slashOthers = addcslashes($_POST['others'], "'");
                $model->update('user', 'Name=\''.$_POST['name'].'\', Firstname=\''.$_POST['firstname'].'\', Phone=\''.$_POST['phone'].'\', Address=\''.$_POST['address'].'\', Others=\''.$slashOthers.'\'', ' WHERE ID ='.$_SESSION['ID']);
                ?>
                <script>
                    document.location.href="http://localhost/projet-2-garderie/profil/index";
                </script>
                <?php

            }
        }

        if(isset($_POST['submitTfa'])) {
            $verif = $_POST['verif'];
            $secretUser = $_POST['secretUser'];
            $userId = $_SESSION['user']['ID'];

            if($tfa->verifyCode($secretUser, $verif)) {
                $model->update('user', 'Secret=\''. $secretUser . '\'', ' WHERE ID ='.$userId);
                $_SESSION['user']['Secret'] = $secretUser;
                ?>
                <script>alert('La double authentification a été activée avec succès')</script>
                <?php
            } else {
                ?>
                <script>alert('Une erreur s\'est produite, veuillez réessayer')</script>
                <?php
            }
        }

        if(isset($_POST['disableTfaUser'])) {
            $userId = $_SESSION['user']['ID'];
            $_SESSION['user']['Secret'] = null;
            $model = new Model();
            $model->update('user', 'Secret= NULL', ' WHERE ID ='.$userId);
        }

        $data = [
          'user'=>$user,
          'child'=>$child,
          'secret' => $secret,
          'qrCode' => $qrCode
        ];
        return $data;
    }
    public static function reservation(){
        $enfant = new Model();
        $enfant = $enfant->select('child', 'WHERE USER_ID = ' . $_SESSION['ID']);
        $enfant = $enfant->fetchAll();





        $data = [
            'enfant'=>$enfant
        ];

        return $data;
    }

}