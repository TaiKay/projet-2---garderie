<?php
/**
 * Created by PhpStorm.
 * User: WEBENOO
 * Date: 24/11/2018
 * Time: 17:40
 */

class Footer
{
    public function __construct()
    {
    }

    public static function graphique(){
        $getParamUrl = $_SERVER['REQUEST_URI'];
        $getParamUrlArray = explode("/", $getParamUrl);
        if ($getParamUrlArray[2] =="administration" && ACTION == 'user'){
            $user = new Model();
            $user = $user->getInstance()->select("user");
            $user = $user->fetchAll();
            $user = count($user);
            $pro = new Model();
            $pro = $pro->select('professional');
            $pro = $pro->fetchAll();
            $pro = count($pro);
            ?>
            <script>
                var chart = AmCharts.makeChart( "chartdiv", {
                    "type": "pie",
                    "theme": "light",
                    "dataProvider": [  {
                        "Inscrit": "Professionnel",
                        "value": <?= $pro;?>
                    }, {
                        "Inscrit": "Utilisateurs",
                        "value": <?= $user;?>
                    }],
                    "valueField": "value",
                    "titleField": "Inscrit",
                    "outlineAlpha": 0.4,
                    "depth3D": 15,
                    "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                    "angle": 30,
                    "export": {
                        "enabled": true
                    }
                } );</script>
            <script>
                var gaugeChart = AmCharts.makeChart( "chartCookie", {
                    "type": "gauge",
                    "theme": "light",
                    "axes": [ {
                        "axisThickness": 1,
                        "axisAlpha": 0.2,
                        "tickAlpha": 0.2,
                        "valueInterval": 20,
                        "bands": [ {
                            "color": "#84b761",
                            "endValue": 90,
                            "startValue": 0
                        }, {
                            "color": "#fdd400",
                            "endValue": 130,
                            "startValue": 90
                        }, {
                            "color": "#cc4748",
                            "endValue": 220,
                            "innerRadius": "95%",
                            "startValue": 130
                        } ],
                        "bottomText": "0 visite sur votre site",
                        "bottomTextYOffset": -20,
                        "endValue": 220
                    } ],
                    "arrows": [ {} ],
                    "export": {
                        "enabled": true
                    }
                } );

                setInterval( randomValue, 2000 );

                // set random value
                function randomValue() {
                    var value = Math.round(<?= $_COOKIE['visite'];?> );
                    if ( gaugeChart ) {
                        if ( gaugeChart.arrows ) {
                            if ( gaugeChart.arrows[ 0 ] ) {
                                if ( gaugeChart.arrows[ 0 ].setValue ) {
                                    gaugeChart.arrows[ 0 ].setValue( value );
                                    gaugeChart.axes[ 0 ].setBottomText( value + " Visite sur votre site" );
                                }
                            }
                        }
                    }
                }
            </script>
            <?php
        }
    }
}