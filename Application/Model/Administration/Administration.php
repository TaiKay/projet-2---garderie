<?php
/**
 * Created by PhpStorm.
 * User: WEBENOO
 * Date: 22/11/2018
 * Time: 14:33
 */

class Administration
{
    public function __construct()
    {
    }
    public static function user(){
        $model = new Model();
        $pro = $model->select('professional','','ORDER BY ID DESC ','LIMIT 5');
        $user = $model->getInstance()->select('user','','ORDER BY ID DESC ','LIMIT 5');
        $data = [
            'pro'=>$pro,
            'user'=>$user
        ];
        return $data;
    }
    public static function manage(){
        $model = new Model();
        $user = $model->getInstance()->select('user','','ORDER BY ID ASC ','LIMIT 5');
        $db = $model;
        $data = [
            'user'=>$user,
            'db'=>$db
        ];
        return $data;
    }

}