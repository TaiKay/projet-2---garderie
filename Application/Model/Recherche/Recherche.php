<?php
/**
 * Created by PhpStorm.
 * User: WEBENOO
 * Date: 22/11/2018
 * Time: 14:26
 */

class Recherche
{
    public function __construct()
    {
    }
    public static function garderie(){

        if(SLUG != '')
        {
            $limitMin = 5 * (intval(SLUG)-1);
            $limitMax = 5;
        }
        else
        {
            $limitMin = 0;
            $limitMax = 5;
        }
        $pro = new Model();
        if(isset($_POST['select']))
        {
            switch ($condition) {
                case 'priceMax':
                    {
                        $pro = $pro->getInstance()->select("professional",$where = '',$orderby ='ORDER BY Price DESC', 'LIMIT '.$limitMin.', '.$limitMax);
                        $pro = $pro->fetchAll();
                        break;
                    }
                case 'priceMin':
                    {
                        $pro = $pro->getInstance()->select("professional",$where = '',$orderby ='ORDER BY Price ASC', 'LIMIT '.$limitMin.', '.$limitMax);
                        $pro = $pro->fetchAll();
                        break;
                    }
                case 'placeMax':
                    {
                        $pro = $pro->getInstance()->select("professional",$where = '',$orderby ='ORDER BY Capacity DESC', 'LIMIT '.$limitMin.', '.$limitMax);
                        $pro = $pro->fetchAll();
                        break;
                    }
                case 'placeMin':
                    {
                        $pro = $pro->getInstance()->select("professional",$where = '',$orderby ='ORDER BY Capacity ASC', 'LIMIT '.$limitMin.', '.$limitMax);
                        $pro = $pro->fetchAll();
                        break;
                    }
            }
        }
        else
        {
            $pro = $pro->getInstance()->select("professional",$where = '', $orderby=' ',  'LIMIT '.$limitMin.', '.$limitMax);
            $pro = $pro->fetchAll();
        }
        $count = new Model();
        $count = $count->getInstance()->requete("SELECT COUNT(*) FROM professional");
        $count = $count->fetch();
        $nbr = intval($count["COUNT(*)"]);
        $pageMax = ceil($nbr/5);
        $slug = new Model();
        $thispage=intval(SLUG);
        foreach ($pro as $value)
        {
            $id = $value['ID'];
            if (isset($_SESSION['Geographical'])){
            $distance[$id] = self::distance($_SESSION['Geographical'] ,$value["Geographical"]);
            }
        }
        if (isset($distance)){
        $data = [
            'test'=>$pro,
            'pageMax'=>$pageMax,
            'thispage'=>$thispage,
            'slug'=>$slug,
            'distance' => $distance
        ];
        }else{
            $data = [
                'test'=>$pro,
                'pageMax'=>$pageMax,
                'thispage'=>$thispage,
                'slug'=>$slug
            ];
        }
        // var_dump($data);
        return $data;
    }

    public static function distance($user, $creche)
   {
       $user = str_replace(' ','',$user);
       $user = str_replace(' ','',$creche);
       $url = "https://graphhopper.com/api/1/route?point=".$user."&point=".$creche."&vehicle=car&locale=de&key=19477ddc-a8a3-440d-a9f2-a37caa7dcb4c";
       $result = file_get_contents($url);
       // var_dump($result);
       $json = json_decode($result, true);
       // var_dump($json);
       $value = $json["paths"][0]["distance"];
       $distance = intval($value);
       $distance = $distance/1000;
       $distance = round($distance, 2);
       return $distance;
   }

}
