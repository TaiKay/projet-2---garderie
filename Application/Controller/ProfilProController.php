<?php
/**
 * Created by PhpStorm.
 * User: WEBENOO
 * Date: 08/11/2018
 * Time: 18:40
 */

class ProfilProController extends Framework
{
    public function index(){
        if ($_SESSION['ROLE'] == 1) {

            include_once MDL_PATH . 'Profil/ProfilPro.php';
            $data = ProfilPro::profilPro();
            $this->render('Layout/ProfilPro', $data);
        }
        else
        {
            $this->render(UNKNOWN_PAGE,[]);

        }

    }

}