<?php
/**
 * Created by PhpStorm.
 * User: WEBENOO
 * Date: 05/11/2018
 * Time: 14:24
 */

class FactureController extends Framework
{
    public function index()
    {
        include_once MDL_PATH.'Facture/Facture.php';
        $sql = Facture::index();

        $this->render('facture',[
        'sql' => $sql
        ]);

    }
    public function detail()
    {
       include_once MDL_PATH.'Facture/Facture.php';
       $facture = Facture::detail(SLUG);
       $month = Facture::Get_Month($facture);
       $child = Facture::Get_Current_Childs_Hours($facture);
       $total = Facture::Get_Total($facture);
       $total_hours = Facture::Get_Hours_Total($child);
       $total_final = Facture::Get_Total_Final($child, $facture);
       $factID = SLUG;

       if(isset($_POST['submit']))
       {
           $_SESSION['FactID'] = $factID;
           ?>
           <script>
               document.location.href="http://localhost/projet-2-garderie/PDF/pdfOutput";
           </script>
           <?php
       }


        $this->render('detailFacture',[
            'sql' => $facture,
            'month' => $month,
            'child' => $child,
            'total' => $total,
            'total_hours' => $total_hours,
            'total_price' => $total_final
        ]);
    }

}
