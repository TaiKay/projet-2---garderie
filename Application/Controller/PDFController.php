<?php
/**
 * Created by PhpStorm.
 * User: WEBENOO
 * Date: 19/11/2018
 * Time: 23:31
 */

class PDFController extends Framework
{
    public function pdfOutput(){
      //  $model = new Model();
      //  $user = $model->select('user','WHERE ID='.$_SESSION['ID']);
     //   $user = $user->fetch();

        include_once MDL_PATH.'Facture/Facture.php';
        $sql = Facture::detail($_SESSION["FactID"]);
        $child = Facture::Get_Current_Childs_Hours($sql);
        $total_hours = Facture::Get_Hours_Total($child);
        $total_final = Facture::Get_Total_Final($child, $sql);

        $fields = array(
            'name' => $sql["user"]["Name"],
            'address' => $sql["user"]["Address"],
            'ZIP' => $sql["user"]["ZIP"],
            'namePro' => $sql["pro"]["Name"],
            'addressPro'  => $sql["pro"]["Address"],
            'ZIPPro' => $sql["pro"]["ZIP"],
            'numeroFacture' => $sql["fact"]["Libelle"],
            'libelle' => 'Garde d\'enfant',
            'prixU' => $sql["fact"]["Price"],
            'date' => $sql["fact"]["Date"],
            'Total' => $total_final,
            'Quantite' => $total_hours,
            'TotalHT' => $total_final

        );


        while ( ob_get_level() ) {     ob_end_clean(); }
        ob_start();
        require_once(ROOT."/public/fpdm/fpdm.php");
        $pdf = new FPDM(ROOT.'/public/pdf/facture_new.pdf');
        $pdf->Load($fields, true);
        $pdf->Merge();
        $pdf->Output("D", "facture-numero.pdf");
        ob_end_flush();


        exit;


    }
}
