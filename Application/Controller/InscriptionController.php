<?php

class InscriptionController extends Framework
{
    public function index()
    {
        $this->render('Inscription',[

        ]);
        include './Application/Model/User/User.php';

        if (isset($_POST['action'])) {
            $data = $_POST['data'];
            foreach ($data as $key => $value) {
                trim($value);
                if (empty($value)) {
                    ?><script>alert('Veuillez remplir le formulaire')</script><?php
                    exit;
                }
            }
            User::register($data);
            ?>

            <?php
        }

        if(isset($_POST['actionPro'])) {
            $data = $_POST['data'];
            foreach ($data as $key => $value) {
                trim($value);
                if (empty($value)) {
                    ?>
                    <script>alert('Veuillez bien remplir le formulaire')</script><?php
                    exit;
                }
            }

            User::registerPro($data);
        }


    }

}
