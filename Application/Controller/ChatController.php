<?php
/**
 * Created by PhpStorm.
 * User: WEBENOO
 * Date: 21/11/2018
 * Time: 11:18
 */

class ChatController extends Framework
{
    public function index(){
        include_once MDL_PATH.'Chat.php';
        $data = Chat::chat();

        $this->render('Chat',$data);
    }

}