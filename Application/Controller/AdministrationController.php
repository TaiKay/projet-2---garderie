<?php
/**
 * Created by PhpStorm.
 * User: WEBENOO
 * Date: 08/11/2018
 * Time: 18:26
 */

class AdministrationController extends Framework
{

    public function user(){
        include_once MDL_PATH.'Administration/Administration.php';
        $data = Administration::user();



        $this->render('Administration',$data);

    }
    public function manage(){
        include_once MDL_PATH.'Administration/Administration.php';
        $data = Administration::manage();


        $this->render('Manage',$data);
    }

}