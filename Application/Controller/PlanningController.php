<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 27/11/2018
 * Time: 13:58
 */

class PlanningController extends Framework
{
    public function detail(){
        include_once MDL_PATH.'Planning/Planning.php';

        $data = Planning::planning();
        $this->render('Planning',$data );
    }

}