<?php
/**
 * Created by PhpStorm.
 * User: WEBENOO
 * Date: 08/11/2018
 * Time: 16:40
 */

class ProfilController extends Framework
{
    public function index(){
        if ($_SESSION['ROLE'] == 2){
            include_once MDL_PATH.'Profil/Profil.php';
            $data = Profil::profil();
            $this->render('Layout/ProfilUser',$data);
        }
        else
        {
            $this->render(UNKNOWN_PAGE,[]);

        }
    }

    public function reservation(){
        include_once MDL_PATH.'Profil/Profil.php';
        $data = Profil::reservation();

        $this->render('Reservation' ,$data);
    }

}