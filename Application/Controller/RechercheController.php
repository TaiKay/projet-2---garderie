<?php
/**
 * Created by PhpStorm.
 * User: WEBENOO
 * Date: 08/11/2018
 * Time: 18:26
 */

class RechercheController extends Framework
{

    public function index(){
        if (isset($_SESSION['ROLE']) && $_SESSION['ROLE'] == 2) {

            include_once MDL_PATH . 'Recherche/Recherche.php';
            $data = Recherche::garderie();


            $this->render('Recherche', $data);
        }elseif (isset($_SESSION['ROLE']) && $_SESSION['ROLE'] == 1){
            ?>
            <script>
                document.location.href="http://localhost:8080<?= PUB_PATH;?>/detail/index/<?= $_SESSION['Name']?>_<?= $_SESSION['IDPRO']?>";
            </script>
<?php
        }
        else{
            $this->render('Layout/404',[]);
        }
    }

}
