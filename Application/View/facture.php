<?php

// var_dump($sql);

if (isset($_POST['month']) && isset($_POST['year']))
{
    $month = $_POST['month'];
    $year = $_POST['year'];
    $result = array();
    $id = 0;
    $cpt = count($sql);
    for ($i=0; $i < $cpt; $i++)
    {
        $date = explode('-', $sql[$i]["Date"]);
        $factYear = intval($date[0]);
        $factMonth = intval($date[1]);
        if($factYear == $year && $factMonth == $month)
        {
            $result[$id]= $sql[$i];
            $id++;
        }
    }
    // if(isset($result))
    //     var_dump($result);
}

?>
<div class="content">
<form class="" action="" method="post">
    <select class="" name="month">
        <option value="01">Janvier</option>
        <option value="02">Fevrier</option>
        <option value="03">Mars</option>
        <option value="04">Avril</option>
        <option value="05">Mai</option>
        <option value="06">Juin</option>
        <option value="07">Juillet</option>
        <option value="08">Aout</option>
        <option value="09">Septembre</option>
        <option value="10">Octobre</option>
        <option value="11">Novembre</option>
        <option value="12">Decembre</option>
    </select>

    <?php
        $thisyear = date("Y");
        $selectYear = intval($thisyear);
        echo "<select class='' name='year'>";
        for ($i=0; $i < 10 ; $i++)
        {
            echo "
                <option value='".$selectYear."'>".$selectYear."</option>
                ";
                $selectYear--;
        }
        echo "</select>";
    ?>
    <input type="submit" name="submit" value="Envoyer">
</form>
<div class="center-align">
    <?php
        if(isset($result))
        {
            foreach ($result as $value)
            {
                $slug = $value['ID'];
                echo "Facture n°: ".$value['Libelle']." => <a href='".PUB_PATH."/facture/detail/".$slug."'>Detail</a>";
            }
        }
     ?>
</div>
</div>
