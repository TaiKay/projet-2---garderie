<form name="formPart" method="post">
<div class="input-field icon_prefix">
    <input id="email" type="text" class="validate" name="data[name]" value="" placeholder="Nom">
    <label for="email">Nom</label>
</div>
<div class="input-field icon_prefix">
    <input id="prenom" type="text" class="validate" name="data[firstname]" value="" placeholder="Prénom">
    <label for="email">Prénom</label>
</div>
<div class="input-field icon_prefix">
    <input id="email" type="email" class="validate" name="data[email]" value="" placeholder="Email">
    <label for="email">Email</label>
    <span class="helper-text" data-error="adresse mail invalide" data-success="adresse mail valide"></span>
</div>
<div class="input-field icon_prefix">
    <input id="password" type="password" class="validate" name="data[password]" value="" placeholder="Mot de passe">
    <label for="password">Mot de passe</label>
</div>
<div class="input-field icon_prefix">
    <input id="confirmpass" type="password" class="validate" name="passwordConfirm" value="" placeholder="Confirmation du mot de passe">
    <label for="password">Confirmation du mot de passe</label>
</div>
<div class="input-field icon_prefix">
    <input id="phone" type="tel" class="validate" name="data[phone]" value="" placeholder="Phone">
    <label for="phone">Numéro de téléphone</label>
</div>
<div class="input-field icon_prefix">
    <input id="address" type="text" class="validate" name="data[address]" value="" placeholder="Adresse">
    <label for="address">Adresse</label>
</div>
<div class="input-field icon_prefix">
    <input id="zipcode" type="text" class="validate" name="data[zip]" value="" placeholder="Code postal">
    <label for="zipcode">Code postal</label>
</div>
<h6>Statut Marital :</h6>
<div class="status center-align">
    <label>
        <input type="radio" id="maried" name="data[status]" value="Marié" class="with-gap" checked>
        <span>Marié</span>
    </label>
    <label>
        <input type="radio" id="single" name="data[status]" value="Célibataire" class="with-gap">
        <span>Célibataire</span>
    </label>
    <label>
        <input type="radio" id="divorced" name="data[status]" value="Divorcé" class="with-gap">
        <span>Divorcé</span>
    </label>
</div>
    <!--<input type="hidden" name="data[verify]" value="gfhjkl" />-->
<div class="center-align">
    <button class="btn waves-effect waves-light" id="submiBtn" type="submit" name="action">Validez
        <i class="material-icons right">send</i>
    </button>
</div>
</form>

