<div class="main-content">
    <div class="formulaire">
        <form class="register col s12" action="" method="post">

            <h3 class="my_title">Activation de votre compte</h3>
            <div class="input-field">
                <input id="icon_prefix" type="email" class="validate" name="data[email]" value="" placeholder="Email">
                <i class="material-icons prefix">add</i>
            </div>
            <div class="input-field">
                <input id="icon_prefix" type="password" class="validate" name="data[code]" value="" placeholder="Clé de validation">
                <i class="material-icons prefix">add</i>
            </div>
            <div class="center-align">
                <button class="btn waves-effect waves-light" type="submit" name="action">Connexion
                    <i class="material-icons right">send</i>
                </button>
            </div>

        </form>
    </div>
</div>