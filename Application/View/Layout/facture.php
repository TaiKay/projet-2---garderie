<div class="main-content">
    <div class="row">
        <div class="col s12 ">
            <?php
            define("TVA", "0");

            $fcm = new Model;
            $fcm = $fcm->select('invoice','WHERE ID = '.SLUG);
            $fcm = $fcm->fetch();

            $expdate = explode('-', $fcm["Date"]);
            $tmpDt = $expdate[1];
            $month = "";

            switch ($tmpDt)
            {
                case '01':
                    $month = "Janvier";
                    break;

                case '02':
                    $month = "Fevrier";
                    break;

                case '03':
                    $month = "Mars";
                    break;

                case '04':
                    $month = "Avril";
                    break;

                case '05':
                    $month = "Mai";
                    break;

                case '06':
                    $month = "Juin";
                    break;

                case '07':
                    $month = "Juillet";
                    break;

                case '08':
                    $month = "Aout";
                    break;

                case '09':
                    $month = "Septembre";
                    break;

                case '10':
                    $month = "Octobre";
                    break;

                case '11':
                    $month = "Novembre";
                    break;

                case '12':
                    $month = "Decembre";
                    break;
            }
            echo "<h4 class='center-align'>Facture du mois de ".$month."</h4>";
            ?>
        </div>
    </div>
    <div class="row" style="margin-top:50px">
        <div class="col s6">
            <?php
                $sFc = new Model;
                $sFc = $sFc->select('invoice', 'WHERE ID = '.SLUG);
                $sFc = $sFc->fetch();
                $proID = $sFc["PROFESSIONAL_ID"];

                $slp = new Model;
                $slp = $slp->select('Professional', 'WHERE ID = '.$proID);
                $slp = $slp->fetch();

                echo "<div style='margin-left: 20px'>".$slp["Name"]."</br>";
                echo $slp["Address"]."</br>";
                echo $slp["SIRET"]."</div>";
                $date = date('d-m-Y',strtotime($sFc['Date']));
                echo "<div style='padding-top: 60px; margin-left:20px;'>Facture n°".$sFc['Libelle']." émise le ".$date."</div>";

             ?>
        </div>
        <div class="col s6">
            <?php
                $slu = new Model;
                $slu = $slu->select('user','WHERE ID ='.$sFc["USER_ID"]);
                $slu = $slu->fetch();

                echo "<div class='left-align' style='float: right; margin-right: 20px;'>".$slu['Name']." ".$slu['Firstname']."</br>";
                echo $slu['Address']."</div>";
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col s1"></div>
        <div class="col s4 borderFac">
            <h6 class="center-align">Désignation</h6>
            <?php echo " - Garde des enfants" ?>
        </div>
        <div class="col s2 borderFac" >
            <h6 class="center-align">Nombre d'heure</h6>
            <?php echo $sFc["Time"] ?>
        </div>
        <div class="col s2 borderFac">
            <h6 class="center-align">Prix Unitaire</h6>
            <?php echo $sFc["Price"] ?>
        </div>
        <div class="col s2 borderFac">
            <h6 class="center-align">Total</h6>
            <?php
                $prix = intval($sFc["Price"]);
                $heures = intval($sFc["Time"]);
                $totalLine = $prix*$heures;

                $total = 0;
                $total += $totalLine;
                echo $totalLine."€"
            ?>
        </div>
        <div class="col s1"></div>
    </div>
    <div class="row">
        <div class="col s6"></div>
        <div class="col s3">
            <div style="float:right">
                <?php echo "Total :"; ?>
            </div>
        </div>
        <div class="col s3">
            <?php echo $total."€"; ?>
        </div>
    </div>

    <div class="row">
        <div class="col s6" style="margin-left:20px;">
            <?php
            function returnLastDay($month)
            {
                $day = 0;
                switch ($month)
                {
                    case '01':
                        $day = 31;
                        break;

                    case '02':
                        if(date('L',strtotime($sFc['Date'])) == true)
                            $day = 29;
                        else
                            $day = 30;
                        break;

                    case '03':
                        $day = 31;
                        break;

                    case '04':
                        $day = 30;
                        break;

                    case '05':
                        $day = 31;
                        break;

                    case '06':
                        $day = 30;
                        break;

                    case '07':
                        $day = 31;
                        break;

                    case '08':
                        $day = 31;
                        break;

                    case '09':
                        $day = 30;
                        break;

                    case '10':
                        $day = 31;
                        break;

                    case '11':
                        $day = 30;
                        break;

                    case '12':
                        $day = 31;
                        break;
                }
                return $day;
            }
            $paiday = returnLastDay($tmpDt);
            $mydate = explode("-", $date);
            $mydate[0] = $paiday;
            $paidDate = implode("-", $mydate);
            // var_dump($mydate);
                echo "Date limite de paiement : ".$paidDate."</br>";
                echo "Mode de règmenent : virement </br>";
                echo "TVA non applicable, art. 293 B du Code Général des Impôts";
             ?>

        </div>
    </div>





</div>
