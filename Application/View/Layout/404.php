


<div id="notfound">
    <div class="notfound">
        <div class="notfound-404">
            <img src="<?= PUB_PATH?>/public/img<?= DIRSEP;?>404.png" alt="">
        </div>
        <h2>Oops! Cette page ne peut etre trouvé</h2>
        <p>Désolée mais cette page est introuvable ou en reparation pour le moment .</p>
        <a href="<?= PUB_PATH?>">Retour a la page d'accueil</a>
    </div>
</div>
