<div class="content">

 <h4> Votre profil</h4>

<div class="container">
    <div class="section">




            <div class="row">
                <div class="card">
                    <div class="card-content">
                        <div class="col s12 m4 l2"></div>
                        <div class="col s12 m4 l4">
                            <ul>
                                <li> Nom  : <?= $user['Name']?> </li>
                                <li>Prenom : <?= $user['Firstname']?> </li>
                                <li>Statut : <?= $user['Status']?></li>
                                <li>Autres : <?= $user['Others']?></li>
                            </ul>
                        </div>

                        <div class="col s12 m4 l4">
                            <ul>
                                <li>Adresse : <?= $user['Address']?></li>
                                <li>Code Postal : <?= $user['ZIP']?></li>
                                <li>Telephone : <?= $user['Phone']?></li>
                            </ul>
                        </div>



                    </div>
                </div>

                <?php
                if ($_SESSION['user']['Secret'] == null) {
                    ?>
                    <a class="waves-effect waves-light btn modal-trigger" href="#modal1">Activer la double authentification</a>
                    <?php
                }
                ?>
                <?php
                if ($_SESSION['user']['Secret'] !== null) {
                    ?>
                    <form method="post" action="">
                        <button type="submit" name="disableTfaUser" class="waves-effect waves-light btn">Désactiver la double authentification</button>
                    </form>
                    <?php
                }
                ?>

                <a class="waves-effect waves-light btn" id="addChild">Ajouter un enfant</a>
                <a class="waves-effect waves-light btn" id="updateInfo">Modifier vos informations</a>

            <div id="modal1" class="modal">
                <form method="post" action="">
                    <div class="modal-content" style="width: 24%; margin: 0 auto">
                        <img src="<?= $qrCode ?>">

                        <input type="text" name="verif"/>
                        <input type="hidden" name="secretUser" value="<?= $secret ?>">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" name="submitTfa" class="waves-effect waves-light btn">Valider</button>
                        <!--<a href="#" class="modal-close waves-effect waves-green btn-flat">Agree</a>-->
                    </div>
                </form>
            </div>

            <script>
                $(document).ready(function(){
                    $('.modal').modal();
                });
            </script>

            <div class="col s12 m4 l2"><p> </p></div>


        </div>

    </div>


    <div class="row" id="childForm" style="display: none">
        <form class="col s12" method="post" action="#">
            <div class="row">
                <div class="input-field col s6">
                    <input id="Name" type="text" class="validate" name="name">
                    <label for="Name"> Nom </label>
                </div>
                <div class="input-field col s6">
                    <input id="Firstname" type="text" class="validate" name="fname">
                    <label for="Firstname">Prenom</label>
                </div>
                <div class="input-field col s6">
                    <input id="Birthday" type="date" class="validate" name="birth">
                    <label for="Birthday">Date de Naissance</label>
                </div>
                <div class="input-field col s6">
                    <input id="Disease" type="text" class="validate" name="disease">
                    <label for="Disease">Maladie</label>
                </div>
                <div class="input-field col s6">
                    <input id="Allergy" type="text" class="validate" name="allergy">
                    <label for="Allergy">Allergie </label>
                </div>
                <button type="submit" name="submit" class="waves-effect waves-light btn">Valider</button>
                <button name="delete" id="delete" type="submit" class="waves-effect waves-light btn">Supprimer</button>
            </div>
        </form>
    </div>

    <div class="row" style="display: none" id="updateForm">
        <form class="col s12" method="post" action="#" enctype="multipart/form-data">
            <div class="row">

                <div class="input-field col s6">
                    <input id="Name" type="text" class="validate" name="name">
                    <label for="Name">Nom</label>
                </div>
                <div class="input-field col s6">
                    <input id="Firstname" type="text" class="validate" name="firstname">
                    <label for="Firstname">Prénom</label>
                </div>
                <div class="input-field col s6">
                    <input id="Phone" type="text" class="validate" name="phone">
                    <label for="Phone">Numéro de téléphone</label>
                </div>
                <div class="input-field col s6">
                    <input id="Address" type="text" class="validate" name="address">
                    <label for="Address">Adresse</label>
                </div>
                <div class="materialize-textarea col s6">
                    <textarea id="Others" class="validate" name="others"></textarea>
                    <label for="Others">Autres</label>
                </div>

                <button type="submit" name="submitInfo" class="waves-effect waves-light btn col s6">Valider</button>
            </div>
        </form>
    </div>

    <script>
        $('#addChild').on('click', () => {
            if ($('#childForm').attr("style")) {
            $('#childForm').fadeIn('slow', () => {
                $('#childForm').removeAttr("style")
        })
        } else {
            $('#childForm').fadeOut('slow', () => {
                $('#childForm').attr("style", "display: none")
        })
        }
        })

        $('#updateInfo').on('click', () => {
            if ($('#updateForm').attr("style")) {
            $('#updateForm').fadeIn('slow', () => {
                $('#updateForm').removeAttr("style")
        })
        } else {
            $('#updateForm').fadeOut('slow', () => {
                $('#updateForm').attr("style", "display: none")
        })
        }
        })
    </script>

</div>

    <div class="container">
        <div class="section">
            <div class="row">
                <?php
                foreach ($child as $children){
                ?>
                <div class="col s4">
                        <div class="window">
                            <div class="overlay"></div>
                            <div class="box profile-header">
                                <img src="https://goo.gl/DM5s4f" alt="" class="profile-img">
                                <h2><?= $children['Firstname'].''.$children['Name'];?></h2>
                                <h4><?= $children['Birthday']?></h4>
                                <h4 style="font-size: 30px !important;"><?= $children['Disease']?></h4>
                                <h4 style="font-size: 30px !important;"><?= $children['Allergy']?></h4>
                            </div>
                            <div class="box profile-footer">
                                <ul class="profile-ul">
                                    <li class="profile-li"><a href="" class="profile"><span class="ion-ios-camera-outline"></span> <?= $children['Disease']?></a></li>
                                    <li class="profile-li"><a href="" class="profile"><span class="ion-ios-heart-outline"></span> <?= $children['Allergy']?></a></li>
                                    <li class="profile-li"><a href="" class="profile"><span class="ion-ios-person-outline"></span> 225M</a></li>
                                </ul>
                                <form action="#" method="post">
                                <button href="" class="btn" name="delete">Delete</button>
                                </form>
                            </div>
                        </div>
                </div>
<?php
                }
                ?>
            </div>
        </div>
    </div>


</div>

