<?php
/**
 * Created by PhpStorm.
 * User: WEBENOO
 * Date: 22/11/2018
 * Time: 15:46
 */
?>
<div id="botHelp" style="display: none">
    <iframe
        allow="microphone;"
        width="350"
        height="430"
        src="https://console.dialogflow.com/api-client/demo/embedded/0f65440e-902b-4905-b45b-b97d313bf9ae">
    </iframe>
</div>
<a class="waves-effect waves-light btn-large" id="help">Un probleme sur notre site ?</a>
<script>
    $('#help').on('click', () => {
        if ($('#botHelp').attr("style")) {
        $('#botHelp').fadeIn('slow', () => {
            $('#botHelp').removeAttr("style")
    })
    } else {
        $('#botHelp').fadeOut('slow', () => {
            $('#botHelp').attr("style", "display: none")
    })
    }
    })
</script>