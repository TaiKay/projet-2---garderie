<div class="main-content content">
    <div class="formulaire">
        <form class="register col s12" action="" method="post">

            <h3 class="my_title">Connexion</h3>
            <div class="input-field">
                <input id="icon_prefix" type="email" class="validate" name="data[email]" value="" placeholder="Email">
                <i class="material-icons prefix">add</i>
            </div>
            <div class="input-field">
                <input id="icon_prefix" type="password" class="validate" name="data[password]" value="" placeholder="Mot de passe">
                <i class="material-icons prefix">add</i>
            </div>
            <div class="center-align">
                <button class="btn waves-effect waves-light" type="submit" name="action">Connexion
                    <i class="material-icons right">send</i>
                </button>
            </div>
            <div class="center-align">
                <button class="btn waves-effect waves-light" type="submit" id="forgot">Mot de passe oublié
                    <i class="material-icons right">send</i>
                </button>
            </div>
        </form>
    </div>
</div>
<script>
    document.getElementById('forgot').addEventListener('click', (e) => {
        e.preventDefault();
        document.location.href = 'http://localhost/projet-2-garderie/user/passwordForgot';
    })
</script>
