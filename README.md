readme :
# Projet 2 - Garderie

 Site de reservation de crèche en ligne.
 
 

Collaborateurs | Nom           | Prénom        | 
------------   | ------------- | ------------- | 
1              | Follin        | Emilie        |
2              | Bosson        | Gauthier      |
3              | Da Costa      | Théo          |
4              | Aubry         | Axel          |





## Cahier des charges 

- [x]  Inscription des utilisateurs et leur authentification
- [x]  Inscription des professionnels et leur authentification
- [x]  Double authentification 
- [x]  Gestion du profil user (modification des infos) 
- [x]  Ajouter des enfants à l'user
- [x]  Gestion du profil pro (modification des infos ) 
- [x]  Ajout des disponibilitées de la crèche
- [x]  Suppression d'un profil (user ou pro)
- [x]  Permettre de reserver une place dans un crèche 
- [ ]  Création d'une facture 
- [x]  Sortie de la facture en PDF



## Consultation du site

- [x] Afficher les crèches
- [x] Afficher le detail d'une crèche 
- [x] afficher les details d'un profil user ou pro
- [x] S'inscrire
- [x] Se désinscrire
- [x] modifier ses infos

## Cas d'utilisation - Rôle: Visiteur

- [x]  Accès a l'inscription
- [x]  Accès page d'accueil


## Cas d'utilisation - Rôle: User
- [x] S'authentifier
- [x] Acceder à son profil
- [x] Ajouter des enfants 
- [x] Gérer le compte utilisateur (modification coordonnées, mot de passe..)
- [x] Rechercher une crèche
- [x] Reserver une place dans une crèche 



## Cas d'utilisation - Rôle: Pro

- [x] S'authentifier
- [x] Gérer le compte professionnel (modification coordonnées, mot de passe...)
- [x] Ajouter des disponibilitées 
- [ ] Consulter des factures 
- [x] Sortir les factures en PDF 


## Cas d'utilisation - Rôle: Administrateur

- [x] Bannir des utilisateurs
- [ ] Désactive le site
- [x] Detail des nouveaux inscrits
- [x] accès au nombre de vue du site



## Mise en place de la Base de données sur une machine Virtuelle
Tutoriel pour mettre en place une base de données sur une machine virtuelle  : 
[PDF](./SQL/TutoVM.pdf)


