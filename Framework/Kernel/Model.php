<?php
/**
 * Created by PhpStorm.
 * User: WEBENOO
 * Date: 07/11/2018
 * Time: 17:21
 */

class Model
{
    private static $connect = null;
    private $bdd;
    private $chat;
    public function __construct(){

        $strBddServeur = "192.168.73.132";
        $strBddLogin = "theo";
        $strBddPassword = "root";
        $strBddBase = "creche";

        $strMsgBase = "messagerie";

        try{
            $this->bdd = new PDO('mysql:host='.$strBddServeur.';dbname='.$strBddBase,$strBddLogin,$strBddPassword,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
            $this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
             $this->chat = new PDO('pgsql:host=localhost;dbname='.$strMsgBase,'postgres','dactheo',array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
             $this->chat->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }
        catch(Exception $e){
            die('Erreur : '.$e->getMessage());
        }
    }
    public function getInstance() {

        if(is_null(self::$connect)) {
            self::$connect = new Model();
        }
        return self::$connect;
    }

    // Permet d'effectuer une requête SQL. Retourne le résultat (s'il y en a un) de la requête sous forme d'objet
    public function requete($req){
        $query = $this->bdd->query($req);


        return $query;

    }
    public function requetePG($req){
        $query = $this->chat->query($req);
        return $query;

    }
    public  function gen_slug($str)
    {
        $a = array('À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ð','Ñ','Ò','Ó','Ô','Õ','Ö','Ø','Ù','Ú','Û','Ü','Ý','ß','à','á','â','ã','ä','å','æ','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ø','ù','ú','û','ü','ý','ÿ','A','a','A','a','A','a','C','c','C','c','C','c','C','c','D','d','Ð','d','E','e','E','e','E','e','E','e','E','e','G','g','G','g','G','g','G','g','H','h','H','h','I','i','I','i','I','i','I','i','I','i','?','?','J','j','K','k','L','l','L','l','L','l','?','?','L','l','N','n','N','n','N','n','?','O','o','O','o','O','o','Œ','œ','R','r','R','r','R','r','S','s','S','s','S','s','Š','š','T','t','T','t','T','t','U','u','U','u','U','u','U','u','U','u','U','u','W','w','Y','y','Ÿ','Z','z','Z','z','Ž','ž','?','ƒ','O','o','U','u','A','a','I','i','O','o','U','u','U','u','U','u','U','u','U','u','?','?','?','?','?','?');
        $b = array('A','A','A','A','A','A','AE','C','E','E','E','E','I','I','I','I','D','N','O','O','O','O','O','O','U','U','U','U','Y','s','a','a','a','a','a','a','ae','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','o','u','u','u','u','y','y','A','a','A','a','A','a','C','c','C','c','C','c','C','c','D','d','D','d','E','e','E','e','E','e','E','e','E','e','G','g','G','g','G','g','G','g','H','h','H','h','I','i','I','i','I','i','I','i','I','i','IJ','ij','J','j','K','k','L','l','L','l','L','l','L','l','l','l','N','n','N','n','N','n','n','O','o','O','o','O','o','OE','oe','R','r','R','r','R','r','S','s','S','s','S','s','S','s','T','t','T','t','T','t','U','u','U','u','U','u','U','u','U','u','U','u','W','w','Y','y','Y','Z','z','Z','z','Z','z','s','f','O','o','U','u','A','a','I','i','O','o','U','u','U','u','U','u','U','u','U','u','A','a','AE','ae','O','o');
        return strtolower(preg_replace(array('/[^a-zA-Z0-9 - _]/','/[ -]+/','/^-|-$/'),array('','-',''),str_replace($a,$b,$str)));
    }
    public function select($table,$where = '',$orderby ='',$limit=''){
        $requete="SELECT * FROM $table ".$where.$orderby.$limit;
        $resultat=$this->bdd->query($requete);
        return $resultat;
    }
    public function delete($table,$where = ''){
        $requete="DELETE  FROM $table". $where;
        $resultat=$this->bdd->query($requete);
        return $resultat;
    }
    public function insert($table,$fields,$data){
        $requete = "INSERT INTO $table "."(".$fields.") VALUES (".$data.")"or die();
        $resultat= $this->bdd->query($requete);
        return $resultat;
    }
    public function update($table , $data , $where =''){
        $requete = "UPDATE ".$table." SET ".$data."".$where;
        $resultat = $this->bdd->query($requete);
        return $resultat;
    }

    // Permet de préparer une requête SQL. Retourne la requête préparée sous forme d'objet
    public function preparation($req){
        $query = $this->bdd->prepare($req);
        return $query;
    }

    // Permet d'exécuter une requête SQL préparée. Retourne le résultat (s'il y en a un) de la requête sous forme d'objet
    public function execution($query, $tab){
        $req = $query->execute($tab);
        return $req;
    }

    // Retourne l'id de la dernière insertion par auto-incrément dans la base de données
    public function dernierIndex(){
        return $this->bdd->lastInsertId();
    }

}
